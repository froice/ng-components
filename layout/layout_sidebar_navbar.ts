import { Component, Input } from '@angular/core';

@Component({
    selector: 'layout-sidebar-navbar',
    templateUrl: './layout_sidebar_navbar.html',
    styleUrls: ['../style.css']
})
export class LayoutAll {

    @Input() public input: any = {};
    @Input() public navbar_items: any[] = [];

    constructor() {

    }
}