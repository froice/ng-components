import { Component, Input, ViewChildren } from '@angular/core';

@Component({
    selector: 'layout-no-nav',
    templateUrl: './layout_no_nav.html',
    styleUrls: ['../style.css', './layout_no_nav.css']
})
export class LayoutNoNav {
    constructor() {

    }
}